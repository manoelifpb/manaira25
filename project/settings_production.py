# -*- coding: utf-8 -*-
from settings import *

DEBUG = False

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

DEFAULT_FROM_EMAIL = 'manaira25 <YOUR_USER@gmail.com>'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'YOUR_USER@gmail.com'
EMAIL_HOST_PASSWORD = 'YOUR_PASSWORD'
EMAIL_SUBJECT_PREFIX = 'manaira25 - '
EMAIL_PORT = 587
EMAIL_USE_TLS = True

INSTAGRAM_FETCH_URL = 'https://api.instagram.com/v1/tags/manaira25ano'\
    's/media/recent/?client_id=2211823774a14d98ae7d0f0bfe4112c4&count'\
    '=1000&callback='

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'manaira25',
        'USER': 'manaira25',
        'PASSWORD': 'manaira25',
        'HOST': '',
        'PORT': '',
    }
}
