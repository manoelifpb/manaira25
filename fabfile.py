# -*- coding: utf-8 -*-
from __future__ import with_statement
from fabric.api import *
from fabric.context_managers import shell_env
import os

env.warn_only = True
pwd = '/deploy/doe_tweet/doe_tweet'
venv_bin = '/deploy/doe_tweet/bin/'
python_bin = venv_bin + 'python'
pip_bin = venv_bin + 'pip'


@hosts('arcos@arcosonline.com.br')
def deploy():

    with cd(pwd):
        # se tiver conflitos, isso ignora
        run('git fetch --all')
        run('git reset --hard origin/master')

    with cd(os.path.join(pwd, 'apps/core/_static/node')):
        run('npm install')
        run('bower install')
        run('grunt deploy')

    with cd(pwd):
        with shell_env(DJANGO_SETTINGS_MODULE='project.settings_production'):
            run('{0} install -r requirements.txt'.format(pip_bin))
            run('{0} manage.py syncdb'.format(python_bin))
            run('{0} manage.py migrate'.format(python_bin))
            run('{0} manage.py collectstatic --noinput'.format(python_bin))
            run('sudo supervisorctl restart doe_tweet')


@hosts('arcos@arcosonline.com.br')
def update_index():
    with cd(pwd):
        with shell_env(DJANGO_SETTINGS_MODULE='project.settings_production'):
            run('{0} manage.py update_index'.format(python_bin))


@hosts('arcos@arcosonline.com.br')
def nginx():
    run('sudo /etc/init.d/nginx restart')


@hosts('arcos@arcosonline.com.br')
def deploy_nginx():
    deploy()
    run('sudo /etc/init.d/nginx restart')


@hosts('arcos@arcosonline.com.br')
def shell():
    open_shell()


@hosts('arcos@arcosonline.com.br')
def deploy_build():
    with cd(os.path.join(pwd, '..')):
        run('ln -s {0} .'.format(os.path.join(pwd, 'supervisor.conf')))
        run('ln -s {0} .'.format(os.path.join(pwd, 'nginx.conf')))

    deploy()

    run('sudo supervisorctl reread')
    run('sudo supervisorctl update')

    nginx()


@hosts('arcos@arcosonline.com.br')
def deploy_mysql():

    run('mysql -uroot -p4rc0s -e "create database doe_tweet;'
        'grant all on doe_tweet.* to doe_tweet@localhost;'
        'use mysql;'
        'update user set '
        'password=password(\'doe_tweet\') where user=\'doe_tweet\';'
        'flush privileges"')


@hosts('arcos@arcosonline.com.br')
def build():
    with cd('/deploy'):
        run('rm -rf doe_tweet')
        run('virtualenv doe_tweet --system-site-packages')

    with cd('/deploy/doe_tweet'):
        run('git clone git@bitbucket.org:arcos-brasil/doe_tweet.git')

    deploy_build()
