from django.views.generic.edit import CreateView
from models import Instagram, Movie, Historia


class IndexView(CreateView):
    model = Historia
    template_name = 'core/index.html'
    success_url = '/?historia=criada'

    def get_context_data(self, **kwargs):
        kwargs['instagram_list'] = Instagram.objects.order_by(
            '-created_at'
        ).filter(
            active=True
        )[:80]

        kwargs['instagram_list_shoing'] = Instagram.objects.order_by(
            '-created_at'
        ).filter(
            active=True
        )[:8]

        kwargs['history_created'] = self.request.GET.get('historia', None)
        kwargs['to_the_form'] = kwargs['history_created'] or \
            kwargs['form'].errors

        kwargs['movie_list'] = Movie.objects.all()
        return super(IndexView, self).get_context_data(**kwargs)


index = IndexView.as_view()
