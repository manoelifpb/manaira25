/*evil, global Swiper, eval */
(function(){
    var $w = $(window),
        $overlays = $('.positioner-show-1, .positioner-show-2, .positioner-show-3'),
        $mainNavLinks = $('.main-nav a');

    $w.on('scroll', function(){
        var half = $w.height() / 2 + $w.scrollTop();

        $overlays.filter(function(){
            return ! $(this).hasClass('reached');
        }).each(function(){

            if ($(this).offset().top < half) {
                $(this).addClass('reached');

                $($(this).data('target')).each(function(i) {
                    (function($this){
                        setTimeout(function(){
                            $this.addClass('show');

                            if ($this.next().not('.show-1').not('.show-2').not('.show-3')) {
                                $this.next().addClass('show');
                            }
                        }, 500 * i);
                    }($(this)));
                });
            }
        });

        var scrollNavLink = $w.scrollTop() * (1920 / $w.width()),
            closer,
            closerDiff = Infinity;
        $mainNavLinks.each(function(){

            var diff = scrollNavLink - parseInt($(this).data('scroll'));

            if (diff < -100) {
                return false;
            }

            if (diff < closerDiff) {
                closerDiff = diff;
                closer = $(this);
            }
        });

        if (closer) {
            $mainNavLinks.not(close[0]).removeClass('active');
            closer.addClass('active');
        }
    });

    $w.trigger('scroll');

    $mainNavLinks.on('click', function(e){
        e.preventDefault();
        $('html, body').animate({scrollTop: (parseInt($(this).data('scroll')) * ($w.width() / 1920))});
    });

    function showVideo() {
        $('.video-wrapper').addClass('show-video');

        if ( $('.video-wrapper iframe').attr('src').indexOf('youtube.com') !== -1 ) {
            $('.video-wrapper iframe').attr('src', $('.video-wrapper iframe').attr('src') + '&autoplay=true');
        }
    }

    $('html').click(function() {
        hideForm();
    });

    $('.modal-conte-sua-historia').on('click', function(e){
        e.stopPropagation();
    });

    function toggleForm() {
        $('.modal-conte-sua-historia').toggleClass('show');
    }

    function hideForm() {
        $('.modal-conte-sua-historia').removeClass('show');
    }

    $('.close-modal').on("click", function(e){
        e.preventDefault();
        hideForm();
    });

    $('.video-wrapper .thumb').on('click', function(e){
        e.preventDefault();
        showVideo();
    });

    $('.video-list a').on('click', function(e){
        e.preventDefault();
        $('.video-wrapper iframe').attr('src', $(this).data('video'));
        showVideo();
    });

    $('.conte-sua-historia-clicker').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        toggleForm();
    });

    if ( window.toTheForm ) {
        $('html, body').animate({scrollTop: parseInt($('.link-form').data('scroll')) * (1920 * 0.8 / $w.width())});
    }

    if ( window.historyCreated ) {
        $('.form-content-wrapper').children().hide().filter('.history-created-wrapper').show();
    }

    $('.back-to-form').on('click', function(e){
        e.preventDefault();
        $('.form-wrapper').show();
        $('.history-created-wrapper').hide();
    });

    $('.regulamento').on('click', function(e){
        e.preventDefault();

        $('.modal-conte-sua-historia').toggleClass('show-regulamento');
    });

    function shuffleArray(array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    }

    var order = [2, 5, 3, 1, 0, 4, 7, 6];
    shuffleArray(order);

    function nextPosition() {
        var position = order.shift();
        order.push(position);
        return position;
    }

    function diff(a, b) {
        var t;
        if (b.length > a.length) {
            t = b;
            b = a;
            a = t;
        }
        return a.filter(function (e) {
            return !b.filter(function(bEl){
                return bEl.pk === e.pk;
            }).length;
        });
    }

    function getRandomInstagram() {
        var possibilities = diff(window.instagramList, window.instagramListShowing);
        // console.log(possibilities);
        var key = Math.floor(Math.random() * possibilities.length);
        return possibilities[key];
    }

    var $instagramLis = $('.swiper-container .li');

    function addInstagramItem(position, instagramModel) {
        window.instagramListShowing[position] = instagramModel;
        var $link = $instagramLis.eq(position).find('a');
        $link.addClass('markForDelete');
        $instagramLis.eq(position).addClass('active');
        var instagramJson = JSON.parse(instagramModel.fields.json);

        var img = new Image();

        img.onload = function() {
            var $img = $(img);
            var $a = $('<a target="_blank" class="new"></a>');
            $a.attr('target', "_blank");
            $img.appendTo($a);
            $a.appendTo($instagramLis.eq(position));
            $img.addClass('instagram-image');

            setTimeout(function(){
                $a.addClass('show');
            }, 100);

            setTimeout(function(){
                $instagramLis.eq(position).find('.markForDelete').remove();
                $a.removeClass('new');
            }, 3100);
        };

        img.src = instagramJson.images.low_resolution.url;

    }

    function instagranize() {
        var position = nextPosition();
        var instagramModel = getRandomInstagram();
        addInstagramItem(position, instagramModel);
    }

    if (window.instagramList.length !== window.instagramListShowing.length) {
        setInterval(instagranize, 1000);
    }


}());