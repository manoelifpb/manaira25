# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns(
    'apps.core.views',

    url(r'^$', 'index', name='core_index'),
    # url(r'^history-create/$', 'historia_create', name='core_historia_create'),
)
