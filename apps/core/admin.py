# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Instagram, Movie, Historia


class InstagramAdmin(admin.ModelAdmin):
    # list_editable = ('active', )
    list_display = (
        'instagram_id', '__unicode__', 'created_at', 'active', 'thumb_image',)


class HistoriaAdmin(admin.ModelAdmin):
    # list_editable = ('active', )
    list_display = (
        'name', 'email', 'phone', 'created_at', 'file',)


admin.site.register(Instagram, InstagramAdmin)
admin.site.register(Movie)
admin.site.register(Historia, HistoriaAdmin)
