# -*- coding: utf-8 -*-
from django.db import models
from jsonfield import JSONField
from embed_video.fields import EmbedVideoField


class Instagram(models.Model):

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    instagram_id = models.TextField(
        u'Instagram Id'
    )

    json = JSONField(
        verbose_name=u'Instagram Json',
        editable=False
    )

    active = models.BooleanField(
        u'Mostrar no Site?',
        default=False
    )

    def thumb_image(self):
        try:
            return '<a href="{}" target="_blank"><img src="{}" /></a>'.format(
                self.json['link'],
                self.json['images']['thumbnail']['url']
            )
        except:
            return ''
    thumb_image.allow_tags = True

    def __unicode__(self):
        try:
            return self.json['caption']['text']
        except:
            return u'post do instagram'

    class Meta:
        verbose_name = 'Postagens do Instagram'
        verbose_name_plural = 'Postagens do Instagram'


class Movie(models.Model):

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    title = models.CharField(
        u'Título',
        max_length=100
    )

    image = models.ImageField(
        u'Imagem',
        upload_to="movies/"
    )

    embed = EmbedVideoField(
        u'Vídeo',
        max_length=250,
        help_text=u'O endereço pode ser de vídeos no youtube ou vimeo.'
    )

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = 'Video'
        verbose_name_plural = 'Videos'


class Historia(models.Model):

    class Meta:
        verbose_name = 'História (Minha História)'
        verbose_name = 'Histórias (Minha História)'

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    name = models.CharField(
        u'Nome',
        max_length=200
    )

    email = models.CharField(
        u'Email',
        max_length=200
    )

    phone = models.CharField(
        u'Telefone',
        max_length=100
    )

    message = models.TextField(
        u'Mensagem'
    )

    file = models.ImageField(
        u'Arquivo',
        upload_to="histories/",
        blank=True
    )
