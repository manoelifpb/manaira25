# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings
import urllib2
import simplejson
from apps.core.models import Instagram


class Command(BaseCommand):

    def handle(self, *args, **options):

        response = urllib2.urlopen(
            settings.INSTAGRAM_FETCH_URL)
        response_json = simplejson.load(response)

        for instagram in response_json['data']:
            Instagram.objects.get_or_create(
                instagram_id=instagram['id'],
                defaults={
                    'json': instagram
                }
            )

        print 'Instagram fetch successfully'
